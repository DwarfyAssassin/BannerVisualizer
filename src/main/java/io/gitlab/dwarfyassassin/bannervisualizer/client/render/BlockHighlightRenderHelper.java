package io.gitlab.dwarfyassassin.bannervisualizer.client.render;

import java.awt.Color;
import org.lwjgl.opengl.GL11;
import io.gitlab.dwarfyassassin.bannervisualizer.client.ClientTickHandler.BlockSide;
import io.gitlab.dwarfyassassin.bannervisualizer.common.BannerVisualizerConfig;
import net.minecraft.client.renderer.Tessellator;
import net.minecraftforge.common.util.ForgeDirection;

public class BlockHighlightRenderHelper {
    private static final Tessellator tessellator = Tessellator.instance;
    
    public static void highlightSide(BlockSide side, Color color, boolean seeThru) {
        float minX = side.x - BannerVisualizerConfig.renderoffset;
        float minY = side.y - BannerVisualizerConfig.renderoffset;
        float minZ = side.z - BannerVisualizerConfig.renderoffset;
        float maxX = side.x + 1 + BannerVisualizerConfig.renderoffset;
        float maxY = side.y + 1 + BannerVisualizerConfig.renderoffset;
        float maxZ = side.z + 1 + BannerVisualizerConfig.renderoffset;

        if(seeThru) GL11.glDisable(GL11.GL_DEPTH_TEST);

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        
        if(side.dir == ForgeDirection.DOWN) draw2DYPlane(minY, minX, minZ, maxX, maxZ, color, false, false); //bottom
        if(side.dir == ForgeDirection.UP) draw2DYPlane(maxY, minX, minZ, maxX, maxZ, color, true, false); //top
        if(side.dir == ForgeDirection.NORTH) draw2DZPlane(minZ, minX, minY, maxX, maxY, color, true, false); //north
        if(side.dir == ForgeDirection.SOUTH) draw2DZPlane(maxZ, minX, minY, maxX, maxY, color, false, false); //south
        if(side.dir == ForgeDirection.WEST) draw2DXPlane(minX, minY, minZ, maxY, maxZ, color, true, false); //West
        if(side.dir == ForgeDirection.EAST) draw2DXPlane(maxX, minY, minZ, maxY, maxZ, color, false, false); //East

        GL11.glDisable(GL11.GL_BLEND);

        if(seeThru) GL11.glEnable(GL11.GL_DEPTH_TEST);
    }
    
    public static void highlightBlock(BlockSide side, Color color, boolean seeThru) {
        highlightBlock(side.x, side.y, side.z, color, seeThru);
    }
    
    public static void highlightBlock(int x, int y, int z, Color color, boolean seeThru) {
        float minX = x - BannerVisualizerConfig.renderoffset;
        float minY = y - BannerVisualizerConfig.renderoffset;
        float minZ = z - BannerVisualizerConfig.renderoffset;
        float maxX = x + 1 + BannerVisualizerConfig.renderoffset;
        float maxY = y + 1 + BannerVisualizerConfig.renderoffset;
        float maxZ = z + 1 + BannerVisualizerConfig.renderoffset;

        if(seeThru) GL11.glDisable(GL11.GL_DEPTH_TEST);

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        
        draw2DYPlane(minY, minX, minZ, maxX, maxZ, color, false, false); //bottom
        draw2DYPlane(maxY, minX, minZ, maxX, maxZ, color, true, false); //top
        draw2DZPlane(minZ, minX, minY, maxX, maxY, color, true, false); //north
        draw2DZPlane(maxZ, minX, minY, maxX, maxY, color, false, false); //south
        draw2DXPlane(minX, minY, minZ, maxY, maxZ, color, true, false); //West
        draw2DXPlane(maxX, minY, minZ, maxY, maxZ, color, false, false); //East

        GL11.glDisable(GL11.GL_BLEND);

        if(seeThru) GL11.glEnable(GL11.GL_DEPTH_TEST);
        
    }
    
    public static void draw2DXPlane(float x, float minY, float minZ, float maxY, float maxZ, Color color, boolean flipt, boolean biDirectional) {
        tessellator.startDrawingQuads();
        if (color != null) tessellator.setColorRGBA(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
        
        if(!flipt || (flipt && biDirectional)) {
            tessellator.addVertex(x, minY, minZ);
            tessellator.addVertex(x, maxY, minZ);
            tessellator.addVertex(x, maxY, maxZ);
            tessellator.addVertex(x, minY, maxZ);
        }
        if(flipt || (!flipt && biDirectional)) { //Draw other way around so that it's visible form both sides. (This causes the normal vector to point the opposite way but it's the same plane)
            tessellator.addVertex(x, minY, maxZ);
            tessellator.addVertex(x, maxY, maxZ);
            tessellator.addVertex(x, maxY, minZ);
            tessellator.addVertex(x, minY, minZ);
        }
        tessellator.draw();
    }
    
    public static void draw2DYPlane(float y, float minX, float minZ, float maxX, float maxZ, Color color, boolean flipt, boolean biDirectional) {
        tessellator.startDrawingQuads();
        if (color != null) tessellator.setColorRGBA(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
        
        if(!flipt || (flipt && biDirectional)) {
            tessellator.addVertex(minX, y, minZ);
            tessellator.addVertex(maxX, y, minZ);
            tessellator.addVertex(maxX, y, maxZ);
            tessellator.addVertex(minX, y, maxZ);
        }
        if(flipt || (!flipt && biDirectional)) { //Draw other way around so that it's visible form both sides. (This causes the normal vector to point the opposite way but it's the same plane)
            tessellator.addVertex(minX, y, maxZ);
            tessellator.addVertex(maxX, y, maxZ);
            tessellator.addVertex(maxX, y, minZ);
            tessellator.addVertex(minX, y, minZ);
        }
        tessellator.draw();
    }

    public static void draw2DZPlane(float z, float minX, float minY, float maxX, float maxY, Color color, boolean flipt, boolean biDirectional) {
        tessellator.startDrawingQuads();
        if (color != null) tessellator.setColorRGBA(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
        
        if(!flipt || (flipt && biDirectional)) {
            tessellator.addVertex(minX, minY, z);
            tessellator.addVertex(maxX, minY, z);
            tessellator.addVertex(maxX, maxY, z);
            tessellator.addVertex(minX, maxY, z);
        }
        if(flipt || (!flipt && biDirectional)) { //Draw other way around so that it's visible form both sides. (This causes the normal vector to point the opposite way but it's the same plane)
            tessellator.addVertex(minX, maxY, z);
            tessellator.addVertex(maxX, maxY, z);
            tessellator.addVertex(maxX, minY, z);
            tessellator.addVertex(minX, minY, z);
        }
        tessellator.draw();
    }

}
