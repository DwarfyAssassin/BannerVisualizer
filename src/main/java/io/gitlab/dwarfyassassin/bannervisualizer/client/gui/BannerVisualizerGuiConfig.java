package io.gitlab.dwarfyassassin.bannervisualizer.client.gui;

import cpw.mods.fml.client.config.GuiConfig;
import io.gitlab.dwarfyassassin.bannervisualizer.common.BannerVisualizer;
import io.gitlab.dwarfyassassin.bannervisualizer.common.BannerVisualizerConfig;
import net.minecraft.client.gui.GuiScreen;

public class BannerVisualizerGuiConfig extends GuiConfig {
    public BannerVisualizerGuiConfig(GuiScreen parent) {
        super(parent, BannerVisualizerConfig.getConfigElements(), BannerVisualizer.MODID, false, false, GuiConfig.getAbridgedConfigPath(BannerVisualizerConfig.config.toString()));
    }
}
