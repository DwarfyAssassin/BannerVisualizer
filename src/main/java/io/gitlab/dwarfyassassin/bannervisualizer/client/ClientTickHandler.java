package io.gitlab.dwarfyassassin.bannervisualizer.client;

import java.awt.Color;
import java.util.*;
import java.util.Map.Entry;
import javax.vecmath.Vector3d;
import org.lwjgl.opengl.GL11;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import io.gitlab.dwarfyassassin.bannervisualizer.client.render.BlockHighlightRenderHelper;
import io.gitlab.dwarfyassassin.bannervisualizer.common.BannerVisualizerConfig;
import io.gitlab.dwarfyassassin.bannervisualizer.common.CommonTickHandler;
import lotr.common.LOTRConfig;
import lotr.common.entity.item.LOTREntityBanner;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.common.util.ForgeDirection;

public class ClientTickHandler extends CommonTickHandler {
    private int lastBannerUpdateTick = 0;
    private Minecraft mc = Minecraft.getMinecraft();
    public static final Vector3d playerPosition = new Vector3d();
    
    public static Map<BlockSide, Color> bannerProtectionsSides = new HashMap<BlockSide, Color>();
    
    
    @SubscribeEvent
    public void onClientTick(ClientTickEvent event) {
        EntityPlayer player = mc.thePlayer;
        
        if(event.phase == Phase.END && player != null && BannerVisualizerKeyHandler.isEnabled && LOTRConfig.allowBannerProtection && player.ticksExisted - lastBannerUpdateTick >= BannerVisualizerConfig.bannerUpdateTime) {
            player.worldObj.theProfiler.startSection("BannerProtectionVisualizerCalc");
            
            bannerProtectionsSides.clear();
            World world = player.worldObj;
            
            List<LOTREntityBanner> banners = world.getEntitiesWithinAABB(LOTREntityBanner.class, player.boundingBox.expand(BannerVisualizerConfig.bannerDetectionRange, BannerVisualizerConfig.bannerDetectionRange, BannerVisualizerConfig.bannerDetectionRange));
            Map<BlockSide, List<LOTREntityBanner>> sides = new HashMap<BlockSide, List<LOTREntityBanner>>();
            
            for(LOTREntityBanner banner : banners) {
                if(!banner.isProtectingTerritory()) continue;
            
                AxisAlignedBB protectionAABB = banner.createProtectionCube();
                ForgeDirection[] highlightSides = BannerVisualizerConfig.getHighlightSides();
                
                for(int x = MathHelper.floor_double(protectionAABB.minX); x <= MathHelper.floor_double(protectionAABB.maxX - 1); x++) {
                    for(int y = MathHelper.floor_double(protectionAABB.minY); y <= MathHelper.floor_double(protectionAABB.maxY - 1); y++) {
                        for(int z = MathHelper.floor_double(protectionAABB.minZ); z <= MathHelper.floor_double(protectionAABB.maxZ - 1); z++) {
                            Block block = world.getBlock(x, y, z);
                            if(block == Blocks.air) continue;
                            
                            for(ForgeDirection dir : highlightSides) {
                                if(!block.shouldSideBeRendered(world, x + dir.offsetX, y + dir.offsetY, z + dir.offsetZ, dir.ordinal())) continue;
                                
                                BlockSide side = new BlockSide(x,y,z, dir);
                                
                                List<LOTREntityBanner> bannerList = sides.get(side);
                                if(bannerList == null) {
                                    bannerList = new ArrayList<LOTREntityBanner>();
                                    sides.put(side, bannerList);
                                }
                                bannerList.add(banner);
                            }
                        }
                    }
                }
                
                for(Entry<BlockSide, List<LOTREntityBanner>> entry : sides.entrySet()) {
                    bannerProtectionsSides.put(entry.getKey(), BannerVisualizerConfig.getHighLightColor(entry.getValue()));
                }
            }

            player.worldObj.theProfiler.endSection();
            lastBannerUpdateTick = player.ticksExisted;
        }
        else if(event.phase == Phase.END && player == null) {
            lastBannerUpdateTick = 0;
        }
    }
    
    @SubscribeEvent
    public void onRender(RenderWorldLastEvent event) {
        EntityPlayerSP player = mc.thePlayer;
        if(player != null) {
            setPlayerData(player, event.partialTicks);

            if(BannerVisualizerKeyHandler.isEnabled) {
                player.worldObj.theProfiler.startSection("BannerProtectionVisualizerDraw");
                
                GL11.glPushMatrix();
                GL11.glDisable(GL11.GL_TEXTURE_2D);

                GL11.glTranslated(-playerPosition.x, -playerPosition.y, -playerPosition.z);
                
                for(Entry<BlockSide, Color> entry : bannerProtectionsSides.entrySet()) {
                    BlockSide side = entry.getKey();
                    
                    if((side.x - playerPosition.x) * (side.x - playerPosition.x) > (mc.gameSettings.renderDistanceChunks * 16) * (mc.gameSettings.renderDistanceChunks * 16)) continue;
                    if((side.y - playerPosition.y) * (side.y - playerPosition.y) > (mc.gameSettings.renderDistanceChunks * 16) * (mc.gameSettings.renderDistanceChunks * 16)) continue;
                    
                    BlockHighlightRenderHelper.highlightSide(side, entry.getValue(), false);
                }
                
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GL11.glPopMatrix();
                
                player.worldObj.theProfiler.endSection();
            }
        }
    }

    public static void setPlayerData(EntityPlayer player, float partialTicks) {
        playerPosition.x = player.lastTickPosX + (player.posX - player.lastTickPosX) * partialTicks;
        playerPosition.y = player.lastTickPosY + (player.posY - player.lastTickPosY) * partialTicks;
        playerPosition.z = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * partialTicks;
    }
    
    public static class BlockSide {
        public int x;
        public int y;
        public int z;
        public ForgeDirection dir;
        
        public BlockSide(int x, int y, int z, ForgeDirection dir) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.dir = dir;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            BlockSide other = (BlockSide) o;
            if (x == other.x && y == other.y && z == other.z && dir == other.dir) return true;

            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y, z, dir);
        }
        
    }
}
