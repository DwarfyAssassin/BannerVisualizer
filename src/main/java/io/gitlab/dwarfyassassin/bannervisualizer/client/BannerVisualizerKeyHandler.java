package io.gitlab.dwarfyassassin.bannervisualizer.client;

import org.lwjgl.input.Keyboard;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import io.gitlab.dwarfyassassin.bannervisualizer.common.CommonTickHandler;
import net.minecraft.client.settings.KeyBinding;

public class BannerVisualizerKeyHandler extends CommonTickHandler {
    private static KeyBinding keyBindingBannerVisualizer = new KeyBinding("Show Protection", Keyboard.KEY_P, "BannerVisualizer");
    public static boolean isEnabled = false;
    
    public BannerVisualizerKeyHandler() {
        FMLCommonHandler.instance().bus().register(this);
        ClientRegistry.registerKeyBinding(keyBindingBannerVisualizer);
    }
    
    @SubscribeEvent
    public void KeyInputEvent(InputEvent.KeyInputEvent event) {
        if(keyBindingBannerVisualizer.isPressed()) {
            isEnabled = !isEnabled;
        }
    
    }
}
