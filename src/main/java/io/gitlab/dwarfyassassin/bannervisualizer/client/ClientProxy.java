package io.gitlab.dwarfyassassin.bannervisualizer.client;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import io.gitlab.dwarfyassassin.bannervisualizer.common.CommonEventHandler;
import io.gitlab.dwarfyassassin.bannervisualizer.common.CommonProxy;

public class ClientProxy extends CommonProxy {
    
    @Override
    public void preInit(FMLPreInitializationEvent event) {
        super.preInit(event);
        new BannerVisualizerKeyHandler();
    }
    
    @Override
    public void registerSidedHandlers() {
        tickHandler = new ClientTickHandler();
        eventHandler = new CommonEventHandler();
    }
}
