package io.gitlab.dwarfyassassin.bannervisualizer.common;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import cpw.mods.fml.client.config.IConfigElement;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import lotr.common.entity.item.LOTREntityBanner;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.config.ConfigCategory;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.util.ForgeDirection;

public class BannerVisualizerConfig {
    public static Configuration config;
    private static List<ConfigCategory> allCategories = new ArrayList<ConfigCategory>();;
    
    private static String CATEGORY_MAIN;
    
    public static int bannerDetectionRange;
    public static int bannerUpdateTime;
    public static int highlightColorType;
    public static String customHighlightColor;
    public static int highlightSide;
    public static float renderoffset;

    private static void setupCategories() {
        CATEGORY_MAIN = makeCategory("main");
    }

    private static String makeCategory(String name) {
        ConfigCategory category = config.getCategory(name);
        category.setLanguageKey(BannerVisualizer.MODID + ".config." + name);
        allCategories.add(category);
        return name;
    }

    public static void setupAndLoad(FMLPreInitializationEvent event) {
        config = new Configuration(event.getSuggestedConfigurationFile());
        setupCategories();
        load();
    }

    public static void load() {
        bannerDetectionRange = config.get(CATEGORY_MAIN, "Banner Detection Range", 64, "The range in block for which banner should be searched.", 1, Integer.MAX_VALUE).getInt();
        bannerUpdateTime = config.get(CATEGORY_MAIN, "Banner Update Time", 20, "The time (in ticks) between refreshing the protection area.", 1, Integer.MAX_VALUE).getInt();
        highlightColorType = config.get(CATEGORY_MAIN, "Highlight Color Type", 0, "The highlight color setting. 0 is protection block type based, 1 is banner faction based, 2 is banner permission based and 3 is custom color.", 0, 3).getInt();
        customHighlightColor = config.get(CATEGORY_MAIN, "Custom Highlight Color", "0x5500FF00", "The custom highlight color in hexadecimal notation in ARGB. This will only be used if the highlight color type is set to custom.", Pattern.compile("^0x[0-9a-fA-F]{8}$")).getString();
        highlightSide = config.get(CATEGORY_MAIN, "Highlight Side", 6, "The Side of the block which should be highlighted. 0 is bottom, 1 is top, 2 is north, 3 is south, 4 is west, 5 is east and 6 is all.", 0, 6).getInt();
        renderoffset = (float) config.get(CATEGORY_MAIN, "Render Offset", 0.01, "The render offset for the block overlay to prevent Z-fighting.", 0, 0.5).getDouble();
        
        if(config.hasChanged()) config.save();
    }
    
    public static List<IConfigElement> getConfigElements() {
        ArrayList<IConfigElement> list = new ArrayList<IConfigElement>();
        for(ConfigCategory category : allCategories) {
            ConfigElement categoryElement = new ConfigElement(category);
            list.add(categoryElement);
        }
        return list;
    }

    public static Color getHighLightColor(List<LOTREntityBanner> banners) {
        List<Color> colors = new ArrayList<Color>();
        
        for(LOTREntityBanner banner : banners) {
            if(highlightColorType == 0) {
                int range = banner.getProtectionRange();
                int color = 0xFFFFFF;
                if(range == 8) color = 0xad4e0a;
                else if(range == 16) color = 0xebf2f2;
                else if(range == 32) color = 0xf2d516;
                
                colors.add(new Color(0x55000000 | color, true));
            }
            else if(highlightColorType == 1) {
                colors.add(new Color(0x55000000 | (banner.getBannerType().faction.getFactionColor() & 0x00FFFFFF), true));
            }
            else if(highlightColorType == 2) {
                colors.add(new Color(0x55000000 | (MathHelper.floor_float(banner.getDefaultPermBitFlags() / 127.0f * 0x00FFFFFF))));
            }
            else {
                return new Color(Integer.valueOf(customHighlightColor.substring(4, 6), 16), Integer.valueOf(customHighlightColor.substring(6, 8), 16), Integer.valueOf(customHighlightColor.substring(8, 10), 16), Integer.valueOf(customHighlightColor.substring(2, 4), 16));
            }
        }
        
        if(colors.size() == 1) return colors.get(0);
        
        int a = 0;
        int r = 0;
        int g = 0;
        int b = 0;
        
        for(Color color : colors) {
            a += color.getAlpha();
            r += color.getRed();
            g += color.getGreen();
            b += color.getBlue();
        }
        
        a /= colors.size();
        r /= colors.size();
        g /= colors.size();
        b /= colors.size();
        
        return new Color(r, g, b, a);
    }
    
    public static ForgeDirection[] getHighlightSides() {
        if(highlightSide == 6) return ForgeDirection.VALID_DIRECTIONS;
        return new ForgeDirection[] {ForgeDirection.VALID_DIRECTIONS[highlightSide]};
    }
}
