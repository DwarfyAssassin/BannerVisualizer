package io.gitlab.dwarfyassassin.bannervisualizer.common;

import cpw.mods.fml.common.event.*;

public class CommonProxy {
    protected CommonTickHandler tickHandler;
    protected CommonEventHandler eventHandler;


    public void preInit(FMLPreInitializationEvent event) {
        registerSidedHandlers();
    }

    public void init(FMLInitializationEvent event) {
    }

    public void postInit(FMLPostInitializationEvent event) {
    }
    
    public void registerSidedHandlers() {
        tickHandler = new CommonTickHandler();
        eventHandler = new CommonEventHandler();
    }

}
