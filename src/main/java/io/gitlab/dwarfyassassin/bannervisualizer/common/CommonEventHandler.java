package io.gitlab.dwarfyassassin.bannervisualizer.common;

import cpw.mods.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class CommonEventHandler {
    public CommonEventHandler() {
        FMLCommonHandler.instance().bus().register(this);
    }
    
    @SubscribeEvent
    public void onConfigChanged(OnConfigChangedEvent event) {
        if(event.modID.equals(BannerVisualizer.MODID)) BannerVisualizerConfig.load();
    }
}
