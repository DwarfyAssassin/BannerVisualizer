package io.gitlab.dwarfyassassin.bannervisualizer.common;

import org.apache.logging.log4j.Logger;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = BannerVisualizer.MODID, name = BannerVisualizer.NAME, version = BannerVisualizer.VERSION, dependencies = "required-after:lotr", acceptableRemoteVersions = "*", guiFactory = "io.gitlab.dwarfyassassin.bannervisualizer.client.gui.BannerVisualizerGuiFactory")
public class BannerVisualizer {
    public static final String MODID = "bannervisualizer";
    public static final String NAME = "Banner Visualizer";
    public static final String VERSION = "1.2.1";
    
    public static Logger log;
    
    @SidedProxy(clientSide = "io.gitlab.dwarfyassassin.bannervisualizer.client.ClientProxy", serverSide = "io.gitlab.dwarfyassassin.bannervisualizer.common.CommonProxy")
    public static CommonProxy proxy;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        log = event.getModLog();
        
        BannerVisualizerConfig.setupAndLoad(event);
        proxy.preInit(event);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }
}
