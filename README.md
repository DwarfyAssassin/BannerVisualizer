Welcome to the Banner Visualizer repository. Banner Visualizer is a minecraft forge mod for LOTRMod by Mevans. It has as goal to add a visual representation of area's protected by banners.

__Instalation__

You can download the mod from gitlab in the [release tab](https://gitlab.com/DwarfyAssassin/BannerVisualizer/-/releases "Go to release tab").

Banner Visualizer can be installed like any other forge mod, just put the mod in your mods folder and it will do it's thing. For it to have any effect you need [LOTR Mod](https://lotrminecraftmod.fandom.com/wiki/The_Lord_of_the_Rings_Minecraft_Mod_Wiki "LOTR Mod wiki page.") by Mevans.

This mod is a forge mod which runs on minecraft 1.7.10.


__Bug & Suggestions__

Any suggestions and or bug reports are welcome in the issues tab.

__How to use__

To show the protected area's press the toggle protection hotkey (P by default). After that you will be able to see which area's are protected and which aren't. In the config you will find multiple setting related to the color, sides and refresh time of the highlighted protection area.