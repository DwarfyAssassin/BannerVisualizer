# Banner Visualizer Changelog

## v1.2.1 - 27/09/2024
### Fixed
- Banner highlighting crash the game for structure banners.

## v1.2.0 - 20/11/2020
### Added
- A render offset config to prevent z-fighting
- Changed the banner F3 highlight so that it highlights your own banners when in survival.

## v1.1.0 - 15/07/2020
### Added
- Added a new highlight color type options which is based on the protection block type
- Added a new config option which lets you chose which sides are highlighted

### Fixed
- Fixed a bug with the permission color type not being transparent

### Changed
- Changed the highlight render overlay offset to be a bit further away from the block

## v1.0.0 - 14/07/2020
### Added
- Added the protection highlight
- Added 3 different color type options and various other config options